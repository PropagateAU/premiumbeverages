angular
    .module('premiumApp', [
        'ui.bootstrap',
        'ngTouch',
        'ngAnimate',
        'ngSanitize'
    ])
    .config(function($interpolateProvider) {
        // Avoid confliction with Phalcon
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    })
    .controller('appCtrl', function($scope, $uibModal, $http, $timeout) {
        var vm = this;
    })
    .controller('authCtrl', function ($scope, $uibModal) {
        $scope.openAgeVerificationModal = function() {
            var requestModalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'ageModal.html',
                controller: 'modalCtrl',
                backdrop: 'static'
            });

            requestModalInstance.result.then(function(data) {
                if (data == 'success') {
                    successModal();
                }
            })
        };

        $scope.openAgeVerificationModal();

        function successModal() {
            $uibModal.open({
                animation: true,
                templateUrl: 'successModal.html',
                controller: 'modalCtrl'
            });
        }
    })
    .controller('modalCtrl', function($scope, $uibModalInstance, $http) {
        var vm = this;
        vm.confirm = false;
        vm.toggleConfirmation = toggleConfirmation;

        function toggleConfirmation() {
            vm.confirm = !vm.confirm;
        }

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
        $scope.confirm = function() {
            $uibModalInstance.close();
        }
    })
    .controller('brandCtrl', function($scope, $uibModal, $http, $timeout) {
        $scope.brandsModal = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'brandsModal.html',
                controller: 'brandsModalCtrl',
                windowClass: 'brands-modal'
            });

            modalInstance.result.then(function (brandSlug) {
                if (brandSlug) {
                    $.fn.fullpage.moveTo(brandSlug, 1);
                }
            }, function(){
                $.fn.fullpage.moveTo(1);
            });
        };
        if(window.location.hash) {
            var brandSlug = window.location.hash.replace('#', '');
            $.fn.fullpage.moveTo(1);
            $timeout(function() {
                $.fn.fullpage.moveTo(brandSlug, 1);
            }, 100);
        } else {
            $scope.brandsModal();
        }

        $scope.openBeerInfo = function(id) {
            var beer = null;


            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'infoModal.html',
                controller: 'beerModalCtrl',
                resolve: {
                    id: function() {
                        return id;
                    }
                }
            });
        };
    })
    .controller('brandsModalCtrl', function($scope, $uibModalInstance, $http) {
        var vm = this;

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
        $scope.confirm = function(brandSlug) {
            $uibModalInstance.close(brandSlug);
        }
    })
    .controller('beerModalCtrl', function($scope, $uibModalInstance, $http, id) {
        var vm = this;

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $http.get('wholesaler_ajax/getBeer/' + id).then(function (response) {
            console.log(response.data.result);
            $scope.beer = response.data.result;
        });
    })
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('clickOnce', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var replacementText = attrs.clickOnce;

                element.bind('click', function() {
                    $timeout(function() {
                        if (replacementText) {
                            element.html(replacementText);
                        }
                        element.attr('disabled', true);
                    }, 0);
                });
            }
        };
    })
