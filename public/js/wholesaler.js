angular
    .module('premiumApp')
    .controller('wholesalerCtrl', function($scope, $uibModal, $http, $timeout) {
        var vm = this;

        $scope.findAU = findAU;
        $scope.findNZ = findNZ;
        $scope.wholesalers = null;
        $scope.country = null;
        $scope.state = null;
        $scope.showAu = true;
        var firstClick = true;
        var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
        $scope.auMapConfig = {
            map: 'au_mill',
            hoverColor: '#cc627d',
            backgroundColor: 'transparent',
            zoomOnScroll: false,
            regionsSelectable: true,
            regionsSelectableOne: true,
            // panOnDrag: false,
            focusOn: {
                x: 1,
                y: 0,
                scale: 1.25
            },
            regionStyle: {
                initial: {
                    "fill": '#f5e0e3',
                    "fill-opacity": 1,
                    "stroke": '#d2758c',
                    "stroke-width": 2,
                    "stroke-opacity": 1
                },
                hover: {
                    "fill": '#be143d',
                    "fill-opacity": 1,
                    "cursor": 'pointer'
                },
                selected: {
                    "fill": '#be143d',
                    "fill-opacity": 1,
                },
                selectedHover: {
                    "fill-opacity": 0.8,
                }
            },
            onRegionClick: function(e, code) {
                findWholesaler(code);
            }
        };

        $scope.nzMapConfig = angular.extend({}, $scope.auMapConfig);
        $scope.nzMapConfig.map = 'oceania_mill';
        $scope.nzMapConfig.focusOn = {
            x: 0,
            y: 0,
            scale: 4
        }
        $scope.nzMapConfig.regionStyle.initial['stroke-width'] = 0.5;
        $scope.nzMapConfig.panOnDrag = false;
        $scope.nzMapConfig.zoomMin = 4;
        $scope.nzMapConfig.selectedRegions = "NZ";

        $scope.init = function(data) {
            $scope.countryCode = data.country;
            initMap(data.country);
        }

        function initMap(country) {
            if (country == 'nz') {
                findNZ()
            } else {
                findAU();
            }
        }

        function findWholesaler(code) {
            $http.get('/wholesaler_ajax/search/' + code).then(function(response) {
                var result = response.data.result;
                $scope.wholesalers = result.wholesalers;
                $scope.country = result.country;
                $scope.state = result.state;
                $('#wholesaler-results').velocity('scroll', {
                    duration: 500,
                    offset: 0,
                    easing: 'ease-in-out'
                });
            })
        }

        function findAU() {
            $scope.showAu = true;
            showMap("#map-au");
            $('#map-au').vectorMap($scope.auMapConfig);
        }

        function findNZ() {
            $scope.showAu = false;
            showMap("#map-nz");
            $('#map-nz').vectorMap($scope.nzMapConfig);
            $('#map-nz').find("g").attr("transform", "scale(2.466666666666667) translate(-378.6486486486498, -402.51034154528645)");
        }

        function showMap(map) {
            var altMap = map == "#map-au" ? "#map-nz" : "#map-au";
            $(altMap).hide();
            $(map).html("");
            $(map).show();
            $(map).closest("path").trigger("click");
            if (map == '#map-nz') {
                findWholesaler("nz");
            }
        }
    })
