<?php

class AuthController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isPost()) {
            $confirm = $this->request->getPost('confirm');

            if ($confirm && $confirm == 'on') {
                $this->session->set('confirmed', true);
                $this->response->redirect('');
            } else {
                $this->response->redirect('auth');
            }
        }
    }

    public function redirectAction($country)
    {
        $this->session->set('country', $country);
        $this->session->set('confirmed', true);
        $this->response->redirect('');
    }
}
