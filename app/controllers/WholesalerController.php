<?php

class WholesalerController extends ControllerBase
{
    public function indexAction()
    {

    }

    public function auAction()
    {
    	$this->view->data = json_encode(["country" => "au"]);
    }

    public function nzAction()
    {
    	$this->view->data = json_encode(["country" => "nz"]);
    }

    public function stockistsAction()
    {

    }

}
