<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateAfter('base');
        $this->view->baseUrl = $this->config->application->baseUri;

        $this->view->menu = $this->getMenu();

        $this->get_location();
        $this->country = $this->session->get('country');
        $this->view->setVars([
            'country'          => $this->country,
            'countryResidence' => $this->country == 'nz' ? 'New Zealand' : 'Australian',
            'auUrl'            => $this->config->application->auUrl,
            'nzUrl'            => $this->config->application->nzUrl,
        ]);
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $confirmed = $this->session->get('confirmed');
        if (!$confirmed) {
            if ($dispatcher->getControllerName() != 'auth') {
                $this->dispatcher->forward(array(
                    "controller" => "auth",
                    "action"     => "index",
                ));
            }
        }
    }

    private function getMenu()
    {
        return [
            [
                "controller" => "",
                "label"      => "Home",
            ],
            [
                "controller" => "brand",
                "label"      => "Our Brands",
            ],
            [
                "controller" => "about",
                "label"      => "About",
            ],
            [
                "controller" => "wholesaler",
                "label"      => "Where To Buy",
            ],
            [
                "controller" => "contact",
                "label"      => "Contact",
            ],
        ];
    }

    private function get_location()
    {
        $country = $this->session->get('country');

        if (!$country) {
            $location = json_decode(file_get_contents('http://freegeoip.net/json/' . $_SERVER['REMOTE_ADDR']), true);
            $country = strtolower($location['country_code']);
            if ($this->config->application->country == 'nz' && $country != 'nz') {
                $this->session->set('country', 'nz');
                $this->response->redirect($this->config->application->auUrl);
                return;
            }

            $country = ($country == 'nz') ? $country : 'au';
            $this->session->set('country', $country);
        }
    }
}
