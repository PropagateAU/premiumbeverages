<?php

class IndexController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        if ($this->request->isPost()) {
            $confirm = $this->request->getPost('confirm');

            if ($confirm && $confirm == 'on') {
                $this->session->set('confirmed', true);
                $this->response->redirect('');
            } else {
                $this->response->redirect('auth');
            }
        }
    }
}
