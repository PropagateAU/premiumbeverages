<?php

class BrandController extends ControllerBase
{
    public function intialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        $beerCollection = $this->getBeerCollection();
        $this->view->beerCollection = $beerCollection;

        $brands = $this->getBrands();
        $this->view->brands = $brands;

        $data = array();

        foreach($beerCollection as $item) {
            $data[] = $item->b->slug;
        }
        $this->view->anchors = json_encode($data);
    }

    public function nzAction() {
        $this->session->set('country', 'nz');
        $this->response->redirect('brand');
    }

    public function auAction() {
        $this->session->set('country', 'au');
        $this->response->redirect('brand');
    }

    private function getBrands()
    {
        $phql = "SELECT b.*, br.* FROM Beer b
                JOIN BrandBeer bb ON b.id = bb.beer_id
                JOIN Brand br ON br.id = bb.brand_id
                JOIN CountryBrand cb ON cb.brand_id = bb.brand_id
                JOIN Country c ON cb.country_id = c.id
                WHERE c.label = :country:";
        if ($this->country == 'au') {
            $phql .= " AND b.au > 0";
        } else if ($this->country == 'nz') {
            $phql .= " AND b.nz > 0";
        }

        $phql .= " GROUP BY br.id ORDER BY cb.position, b." . $this->country;

        $result = $this->modelsManager->executeQuery($phql, ["country" => $this->country]);
        return $result;
    }

    private function getBeerCollection()
    {
        $phql = "SELECT b.*, br.* FROM Beer b
                JOIN BrandBeer bb ON b.id = bb.beer_id
                JOIN Brand br ON br.id = bb.brand_id
                JOIN CountryBrand cb ON cb.brand_id = bb.brand_id
                JOIN Country c ON cb.country_id = c.id
                WHERE c.label = :country:";
        if ($this->country == 'au') {
            $phql .= " AND b.au > 0";
        } else if ($this->country == 'nz') {
            $phql .= " AND b.nz > 0";
        }

        $phql .= " ORDER BY cb.position, b." . $this->country;

        $result = $this->modelsManager->executeQuery($phql, ["country" => $this->country]);
        return $result;
    }
}

