<?php

use Phalcon\Mvc\View;

class WholesalerAjaxController extends ControllerAjax
{
    public function getBeerAction($id)
    {
        $this->view->result = Beer::findFirstById($id);
    }

    public function searchAction($code)
    {

        $region = $this->mapCode($code);
        $data['country'] = $region['country'];
        $data['state'] = $region['state'];

        $conditions = "country = '" . strtoupper($region['countryCode']) . "'";
        if (isset($region['state']) && $region['state'] != "") {
            $conditions .= " AND state = '" . strtoupper($region['stateCode']) . "'";

        }
        $data['wholesalers'] = Wholesaler::find($conditions)->toArray();

        $this->view->result = $data;
    }

    public function mapCode($code)
    {
        switch ($code) {
            case "AU-VIC":
                return array(
                    "state"       => "Victoria",
                    "stateCode"   => "vic",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-NSW":
                return array(
                    "state"       => "New South Wales",
                    "stateCode"   => "nsw",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-SA":
                return array(
                    "state"       => "South Australia",
                    "stateCode"   => "sa",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-NT":
                return array(
                    "state"       => "Northern Territory",
                    "stateCode"   => "nt",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-QLD":
                return array(
                    "state"       => "Queensland",
                    "stateCode"   => "qld",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-TAS":
                return array(
                    "state"       => "Tasmania",
                    "stateCode"   => "tas",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            case "AU-WA":
                return array(
                    "state"       => "Western Australia",
                    "stateCode"   => "wa",
                    "country"     => "Australia",
                    "countryCode" => "au",
                );
            default:
                return array(
                    "state"       => null,
                    "stateCode"   => null,
                    "country"     => "New Zealand",
                    "countryCode" => "nz",
                );
        }
    }

}
