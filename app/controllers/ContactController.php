<?php

class ContactController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isPost()) {
            $request = $this->request->getPost();
            $errors = $this->validatePostData($request);
            if ($errors) {
                $errors = implode(",", $errors);
                $this->flash->error($errors);
                return;
            }

            $data = $this->mapPostData($request);
            $this->mail->send(
                [$this->config->mail->toEmail => $this->config->mail->toEmail],
                'New Contact Form Submission at premiumbeverages.com.au',
                'contact',
                ['data' => $data]
            );

            $this->flash->success("Your message has been sent successfully!");
            $this->response->redirect('contact');
        }
    }

    private function getRequiredFields()
    {
        return [
            "first_name",
            "last_name",
            "email",
            "phone",
            "company",
            "state",
            "postcode",
            "message_type",
            "message",
        ];
    }

    private function mapPostData($request)
    {
        $data = [
            'First Name'   => $request['first_name'],
            'Last Name'    => $request['last_name'],
            'Email'        => $request['email'],
            'Phone'        => $request['phone'],
            'Company'      => $request['company'],
            'State'        => $request['state'],
            'Post code'    => $request['postcode'],
            'Message Type' => $request['message_type'],
            'Message'      => $request['message'],
        ];

        return $data;
    }

    private function validatePostData($request)
    {
        $requiredFields = $this->getRequiredFields();

        $errors = array();
        foreach ($request as $k => $v) {
            if (!in_array($k, $requiredFields)) {
                $errors[] = $k;
            }
        }

        return count($errors) > 0 ? $errors : false;
    }

}
