<section>
    <div class="container-fluid">
        <div class="row">
            <div class="slick-carousel">
                <div class="hero-wrapper" style="background-image:url('img/banner/Banner01.jpg')">
                    <div class="container flex-wrapper-space-between">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="hero-text-wrapper hero-text--more hidden-xs hidden-sm hidden-md">
                                    <h1 class="hero-title">More To The Table</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 flex-wrapper">
                                <div class="hero-text-wrapper hero-text--more hidden-xs hidden-sm hidden-md">
                                    <h1>Meet the family</h1>
                                    <p>Exceptional quality and consistency run in the Coopers family. These traits enable each family member to shine and display its own unique character. We look for the same qualities in our hand-picked selection of international
                                        partners.
                                    </p>
                                </div>
                                <div class="flex-wrapper__coopers">
                                    <div class="hero-text-wrapper beer-slick">
                                        <div class="beer-slick__text">
                                            <h1>COOPERS AUSTRALIAN MADE AND AUSTRALIAN OWNED SINCE 1862</h1>
                                            <div class="btn-wrapper">
                                                <a href="brand#coopers-original-pale-ale">VIEW COOPERS</a>
                                            </div>
                                        </div>

                                        <div class="beer-slick__img">
                                            <img src="img/brands/brand-coopers-hover.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero-wrapper" style="background-image:url('img/banner/Banner02.jpg')">
                    <div class="container carlsberg-wrapper">
                        <div class="hero-text-wrapper hero-text hero-text--center">
                            <div class="beer-slick beer-slick--column beer-slick--carlsberg">
                                <div class="beer-slick__img">
                                    <img src="img/brands/brand-carlsberg-hover.png">
                                </div>
                                <div class="beer-slick__text">
                                    <h1>CARLSBERG DANISH DESIGNED SINCE 1847</h1>
                                    <div class="btn-wrapper">
                                        <a href="brand#carlsberg-larger">VIEW CARLSBERG</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero-wrapper" style="background-image:url('img/banner/Banner03.jpg')">
                    <div class="container sapporo-wrapper">
                        <div class="hero-text-wrapper hero-text hero-text--center">
                            <div class="beer-slick">
                                <div class="beer-slick__img">
                                    <img src="img/brands/brand-sapporo-home.png">
                                </div>
                                <div class="beer-slick__text">
                                    <h1>SAPPORO THE LEGENDARY BIRU OF JAPAN</h1>
                                    <div class="btn-wrapper">
                                        <a href="brand#sapporo-lager">VIEW SAPPORO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="col-wrapper col-wrapper--home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-fixed-height col-padding-md">
                <h2>Want to stock the worlds best brands</h2>
                <p>If you are interested in stocking any of our portfolio let us connect you to a wholesaler in your area.</p>
                <div class="btn-wrapper">
                    <a href="/wholesaler">Find Wholesalers</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-fixed-height col-padding-md">
                <h2>Bringing the world's best home</h2>
                <p>We proudly manage and distribute the revered stable of Coopers products along with a hand picked portfolio that includes a selection of the world’s best beer and cider brands.</p>
                <div class="btn-wrapper">
                    <a href="/brand">View Our Range</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-fixed-height col-padding-md">
                <h2>From the brewery to beyond</h2>
                <p>Discover how we support our stockists and customers.</p>
                <div class="btn-wrapper">
                    <a href="/about">About Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('document').ready(function() {
        $('.slick-carousel').slick({
            dots: false,
            slidesToShow: 1,
            arrows: true,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    });
</script>
