<main id="fullPage" ng-controller="brandCtrl">
    {% for beer in beerCollection %}
    <div class="section" data-anchor="{{beer.b.slug}}">
        {{ partial('partials/product-slide', ['beer': beer]) }}
    </div>
    {% endfor %}
</main>

<script type="text/ng-template" id="brandsModal.html">
    {% include 'partials/brands-modal.volt' %}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#fullPage').fullpage({
            anchors: {{anchors}},
            verticalCentered: true,
            controlArrows: true,
            autoScrolling: true,
            css3: true,
            menu: '#menu',
            scrollingSpeed: 1000,
            paddingTop: '150px',
            // scrollOverflow: true,
            // scrollOverflowReset: true,
            afterLoad: function(anchorLink, index) {
                $('.section').each(function() {
                    $(this).removeClass('animation-leaving');
                })
                $('.section').eq(index - 1).addClass('animation-active');
            },
            onLeave: function(index, nextIndex, direction) {
                $('.section').eq(index - 1).removeClass('animation-active');
                if (index > 1) {
                    $('.section').eq(index - 1).addClass('animation-leaving');
                }
            }
        });
    });
    $(".fp-prev").click(function() {
        $.fn.fullpage.moveSectionUp();
    })
    $(".fp-next").click(function() {
        $.fn.fullpage.moveSectionDown();
    })
</script>
