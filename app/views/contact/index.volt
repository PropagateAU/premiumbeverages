<section class="col-wrapper col-wrapper--contact">
    <div class="container">
        <div clas="row">
            <div class="title-wrapper title-wrapper--border-bottom">
                <h2>Contact Us</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-padding-lg">
                <h1>Get in<br/>touch<br/>with us</h1>
                <p>We welcome your feedback and look forward to hearing from you. Please be sure to include all of your details and select the most appropriate subject options so we can put you in touch directly with a team member from the relevant department.</p>
                <div class="office-wrapper">
                    <p>
                        <strong>Head Office</strong><br/> 2/11 Sabre Drive,<br/> Port Melbourne<br/> Australia, 3207
                    </p>
                </div>
                <p>
                    <span class="icon-wrapper icon-telephone">&nbsp;</span><a href="#"> {% if country != 'au' %}0061 3{% else %}(03){% endif %} 9245 1900</a><br/>
                    <span class="icon-wrapper icon-envelope-open">&nbsp;</span><a href="#">reception@premiumbeverages.com.au</a>
                </p>

            </div>
            <div class="col-xs-12 col-md-6">
                <form class="form-wrapper" method="post" action="">
                    <div class="form-group">
                        <input type="text" class="form-control" name="first_name" placeholder="First Name *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="last_name" placeholder="Last Name *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="company" placeholder="Company *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" placeholder="Phone *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="state" placeholder="State *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="postcode" placeholder="Post code *" required>
                    </div>
                    <div class="form-group" placeholder="Select Message Type">
                        <select class="form-control" name="message_type" placeholder="Phone *" required>
                            <option selected>Please select message type</option>
                            <option value="Sales Enquiry">Sales Enquiry</option>
                            <option value="Product Enquiry">Product Enquiry</option>
                            <option value="Sponsorship & Donations">Sponsorship & Donations</option>
                            <option value="Nutrition or Ingredients">Nutrition or Ingredients</option>
                            <option value="Product Complaint">Product Complaint</option>
                            <option value="Other enquiries">Other enquiries</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <textarea rows="5" class="form-control" name="message" placeholder="Message *" required></textarea>
                    </div>
                    <div class="form-group">
                        {{ flash.output() }}
                    </div>
                    <div class="form-group">
                        <div class="btn-wrapper">
                            <button type="submit" class="btn btn-premium">Submit</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
