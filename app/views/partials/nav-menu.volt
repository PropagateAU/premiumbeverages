<nav id="mobile-nav" class="overlay">
    <div class="menu-header">
        <h1>menu.</h1>
    </div>
    <div class="menu-line">
        <hr>
    </div>
    <!-- Overlay content -->
    <div class="menu-items">
        {% for item in menu %}
        <div><a href="{{baseUrl}}{{item['controller']}}">{{item['label']}}</a></div>
        {% endfor %}
        <div class="menu-terms"><a href="{{baseUrl}}terms">Terms</a></div>
    </div>
    <div class="menu-line">
        <hr>
    </div>
</nav>
<!-- Close Menu Overlay -->
<!-- Use any element to open/show the overlay navigation menu -->
<div class="menu-icon menu-ham">
    <button class="c-hamburger c-hamburger--htx"><span>toggle menu</span></button>
</div>
<script type="text/javascript">
    (function() {
        "use strict";
        $(".c-hamburger").each(function () {
            $(this).on("click", function(e) {
                e.preventDefault();
                $(this).toggleClass("is-active");
                toggleNav();
            });
        })

        function openNav() {
            $("body").addClass("menu-open");
        }
        function closeNav() {
            $("body").removeClass("menu-open");
        }

        function toggleNav() {
            if ($("#mobile-nav").height() == "0") {
                openNav();
            } else {
                closeNav();
            }
        }
    })();
</script>
