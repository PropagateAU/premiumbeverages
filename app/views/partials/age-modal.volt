<!-- Modal Demo -->
<div id="age-verification-modal" class="modal-content" ng-show="!success">
    <div class="modal-header text-center">
        <img src="img/pb-logo.png">
    </div>
    <div class="modal-body">
        <!-- Modal Content Open -->
        <div>
            <form action="/" method="post">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h3 class="uppercase">Verify Your Age</h3>
                        <p>By entering this site you acknowledge that you are over the age of 18 and you have read and understood our <a href="{{termsUrl}}">terms & conditions</a> and <a href="{{policyUrl}}">privacy policy<a/></p>
                        <div class="form-group checkbox-wrapper">
                            <div class="btn-group" data-toggle="buttons" ng-click="vm.toggleTrial()">
                                <label class="btn" ng-class="{'active': vm.confirm}">
                                    <input type="checkbox" name="confirm" ng-model="vm.confirm">
                                    <i class="zmdi zmdi-check"></i>
                                </label>
                            </div>
                            I am at least 18 years old.
                        </div>

                        <div class="btn-wrapper">
                            <button class="btn" type="submit" href="">Submit</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--Content Close -->
</div>
