<div class="container brand-wrapper">
    <div class="fp-controlArrow fp-prev"></div>
    <div class="fp-controlArrow fp-next"></div>
    <div class="beer-slide">
        <div class="beer-img-wrapper" style="background-image: url('img/beers/{{beer.b.image}}')">
        </div>
        <div class="stats-wrapper">
            <!-- Stats Start -->
            {% if beer.b.stats_alc_per_vol %}
            <div class="stats-box">
                <div class="stats-value">{{beer.b.stats_alc_per_vol}}<sup>{{beer.b.stats_alc_per_vol_unit}}</sup></div>
                <div class="stats-label">ALC/VOL</div>
            </div>
            {% endif %} {% if beer.b.stats_small_vol AND beer.b.stats_small_vol != " " %}
            <div class="stats-box">
                <div class="stats-value">{{beer.b.stats_small_vol}}<sup>{{beer.b.stats_small_vol_unit}}</sup></div>
                <div class="stats-label">{{beer.b.stats_small_pack_label}} {{beer.b.stats_small_pack}}</div>
            </div>
            {% endif %} {% if beer.b.stats_vol AND beer.b.stats_vol != " " %}
            <div class="stats-box">
                <div class="stats-value">{{beer.b.stats_vol}}<sup>{{beer.b.stats_vol_unit}}</sup></div>
                <div class="stats-label">{{beer.b.stats_pack_label}} {{beer.b.stats_pack}}</div>
            </div>
            {% endif %} {% if beer.b.stats_large_vol AND beer.b.stats_large_vol != " " %}
            <div class="stats-box">
                <div class="stats-value">{{beer.b.stats_large_vol}}<sup>{{beer.b.stats_large_vol_unit}}</sup></div>
                <div class="stats-label">{{beer.b.stats_large_pack_label}} {{beer.b.stats_large_pack}}</div>
            </div>
            {% endif %} {% if beer.b.stats_keg_vol AND beer.b.stats_keg_vol != " " %}
            <div class="stats-box">
                <div class="stats-value">{{beer.b.stats_keg_vol}}<sup>{{beer.b.stats_keg_vol_unit}}</sup></div>
                <div class="stats-label">{{beer.b.stats_keg_label}}</div>
            </div>
            {% endif %}
            {% if beer.b.roll_bottle AND beer.b.roll_bottle == 1 %}
            <div class="stats-box" style="background-image: url('img/textures/{{beer.b.roll_bottle_image}}'">
                <div class="stats-value">
                    {% if beer.b.roll_bottle_dark == 1 %}
                        <img class="roll-bottle" src="img/beers/RollBottleWhite.png">
                    {% else %}
                        <img class="roll-bottle" src="img/beers/RollBottle.png">
                    {% endif %}
                </div>
                <div class="stats-label {% if beer.b.roll_bottle_dark == 1 %} stats-light {% endif %}">
                {% if beer.b.roll_bottle AND beer.b.roll_bottle == 1 %}ROLL BOTTLE{% endif %}</div>
            </div>
            {% endif %}
        </div>
        <!-- Stats End -->
        <div class="beer-tagline-wrapper">
            <!-- Right Text -->
            <h1 class="beer-tagline">{{beer.b.tagline}}</h1>
        </div>
        <div class="beer-slide__description">
            <!-- Right Text End -->
            <div class="beer-title">
                <!-- Beer Name -->
                <h1>{{beer.b.title}}</h1>
            </div>
            <div class="beer-info hidden-xs hidden-sm hidden-md">
                <p>{{beer.b.description}}</p>
            </div>
            <div class="beer-more hidden-lg">
                <a ng-click="openBeerInfo('{{ beer.b.id}}')">more <span class="icon-info-circle"></span></a>
            </div>
            <div class="brand-btn-wrapper hidden-sm hidden-md hidden-lg">
                <button class="btn-brand">BRANDS</button>
            </div>
            <div class="icon-scroll-wrapper">
                <span class="icon-mouse-scroll"></span>
            </div>
        </div>
    </div>
</div>
<script type="text/ng-template" id="infoModal.html">
    {% include 'partials/info-modal.volt' %}
</script>
