<!-- Modal Demo -->
<div id="brands-modal" class="modal-content" ng-show="!success">
    <div class="modal-header text-center">
        <h3 class="uppercase">[[beer.title]]</h3>
        <div class="btn-close" ng-click="cancel()"><img src="img/close.png" class="img-responsive"></div>

    </div>
    <div class="modal-body">
        <!-- Modal Content Open -->
        <div class="row">
            <div class="col-xs-12">
                <p>[[beer.description]]</p>
            </div>
        </div>
    </div>
    <!--Content Close -->
</div>
