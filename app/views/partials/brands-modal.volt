<!-- Modal Demo -->
<div id="brands-modal" class="modal-content" ng-show="!success">
    <div class="modal-header text-center">
        <h1 class="uppercase">Our Brands</h1>
        <div class="btn-close" ng-click="cancel()"><img src="img/close.png" class="img-responsive"></div>

    </div>
    <div class="modal-body">
        <!-- Modal Content Open -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 brand-flex-wrapper">
                    {% for brand in brands %}
                    <div class="col-xs-12 col-sm-6 col-lg-3 text-center">
                        <a id="brand-{{brand.br.id}}" ng-click="confirm('{{brand.b.slug}}')" class="brand-hover" href="">
                            <div class="brand-logo-wrapper" style="background-image: url('img/brands/{{brand.br.image}}')">
                                <img class="image" src="img/brands/{{brand.br.image}}" style="opacity:0;">
                            </div>
                        </a>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
    <!--Content Close -->
</div>

<script>
    {% for brand in brands %}
        $('#brand-' + {{brand.br.id}}).hover(function() {
            $(this).find(".brand-logo-wrapper").css("background-image", "url('img/brands/{{brand.br.hover_image}}')");
        }, function() {
            $(this).find(".brand-logo-wrapper").css("background-image", "url('img/brands/{{brand.br.image}}')");
        })
    {% endfor %}
</script>
