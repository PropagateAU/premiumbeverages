<header id="menu">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 header-wrapper">
                <div class="country-wrapper">
                    <span uib-dropdown>
                        <a id="simple-dropdown" uib-dropdown-toggle>
                            <span class="flag-icon flag-icon-{{country}}"></span>
                        </a>
                        <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                            <li>
                                <a href="{{auUrl}}auth/redirect/au">
                                    <span class="flag-icon flag-icon-au"></span> AU
                                </a>
                                <a href="{{nzUrl}}auth/redirect/nz">
                                    <span class="flag-icon flag-icon-nz"></span> NZ
                                </a>
                            </li>
                        </ul>
                    </span>
                </div>
                <div class="logo-wrapper">
                    <a href="{{baseUrl}}"><img src="{{baseUrl}}img/pb-logo.png" class="img-responsive"></a>
                </div>
                <div class="nav-wrapper hidden-xs hidden-sm">
                    <nav>
                        {% for item in menu %}
                            <a href="{{baseUrl}}{{item['controller']}}" {% if router.getControllerName() == item['controller'] %} class="active"{% endif %}>{{item['label']}}</a>
                        {% endfor %}
                    </nav>
                </div>
                <div class="menu-wrapper hidden-md hidden-lg">
                    {% include 'partials/nav-menu.volt' %}
                </div>
            </div>
        </div>
    </div>
</header>