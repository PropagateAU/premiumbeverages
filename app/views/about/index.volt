<section class="carousel-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="hero-wrapper hero-wrapper--page" style="background-image:url( 'img/bg-about.png')">
                <div class="container">
                    <div class="hero-text-wrapper hero-text hero-text--center">
                        <h1>PART OF THE COOPERS FAMILY TREE</h1>
                        <p>Premium Beverages is the distribution arm of the business. 100% Australian-owned, we manage the revered stable of Coopers products. Plus to satisfy {{countryResidence}} consumers’ increasingly adventurous palates, our portfolio includes a selection of the world’s best beer and cider brands.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h1 class="col-padding-xlg">From The Brewery To Beyond</h1>
                <p class="text-wrapper--about col-padding-lg">We’re big enough to provide the support, knowledge and value you look for in a beverage partner. But as a genuine family-owned and operated business, the Premium Beverages team remains committed to the little things. The personal touches that mean you get the service and range to satisfy you and your customers’ needs. </p>

            </div>
            <div class="col-xs-12 col-md-6">
                <div class="beer-text-wrapper">
                    <div class="beer-wrapper">
                        <img src="img/beer-icon.png">
                    </div>
                    <div class="beer-wrapper">
                        <h3>DRAUGHT SERVICES AND SUPPORT.</h3>
                    </div>
                </div>
                <div class="beer-text-wrapper">
                    <div class="beer-wrapper">
                        <img src="img/beer-icon.png">
                    </div>
                    <div class="beer-wrapper">
                        <h3>STAFF TRAINING AND BEER EDUCATION.</h3>
                    </div>
                </div>
                <div class="beer-text-wrapper">
                    <div class="beer-wrapper">
                        <img src="img/beer-icon.png">
                    </div>
                    <div class="beer-wrapper">
                        <h3>PERSONAL ACCOUNT MANAGEMENT.</h3>
                    </div>
                </div>
                <div class="beer-text-wrapper">
                    <div class="beer-wrapper">
                        <img src="img/beer-icon.png">
                    </div>
                    <div class="beer-wrapper">
                        <h3>Local area marketing support for your venue or store.</h3>
                    </div>
                </div>
                <div class="beer-text-wrapper">
                    <div class="beer-wrapper">
                        <img src="img/beer-icon.png">
                    </div>
                    <div class="beer-wrapper">
                        <h3>Equipment and infrastructure support for your venue</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>