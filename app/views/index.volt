<!DOCTYPE html>
<html lang="en" ng-app="premiumApp">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags #must# come first in the head; any other head content must come #after# these tags -->
    <title>Premium Beverages</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    {{ stylesheet_link('node_modules/bootstrap/dist/css/bootstrap.min.css') }}
    {{ stylesheet_link('node_modules/bootstrap/dist/css/bootstrap-theme.min.css') }}
    {{ stylesheet_link('node_modules/slick-carousel/slick/slick.css') }}
    {{ stylesheet_link('node_modules/slick-carousel/slick/slick-theme.css') }}
    {{ stylesheet_link('node_modules/fullpage.js/dist/jquery.fullpage.css') }}
    {{ stylesheet_link('node_modules/flag-icon-css/css/flag-icon.css') }}

    {{ stylesheet_link('js/jquery-jvectormap.css') }}
    {{ stylesheet_link('style.css') }}
    {{ stylesheet_link('css/style.css') }}

    {{ javascript_include('node_modules/jquery/dist/jquery.min.js') }}
    {{ javascript_include('node_modules/slick-carousel/slick/slick.min.js') }}
    {{ javascript_include('node_modules/fullpage.js/dist/jquery.fullpage.min.js') }}
    {{ javascript_include('node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js') }}
    {{ javascript_include('node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js') }}
    {{ javascript_include('node_modules/velocity-animate/velocity.min.js') }}
    {{ javascript_include('node_modules/velocity-animate/velocity.ui.min.js') }}
    {{ javascript_include('js/jquery-jvectormap-2.0.3.min.js') }}
    {{ javascript_include('js/jquery-jvectormap-au-mill.js') }}
    {{ javascript_include('js/jquery-jvectormap-nz-mill.js') }}
    {{ javascript_include('js/jquery-jvectormap-oceania-mill.js') }}


</head>

<body ng-controller="appCtrl" ng-cloak>
    {{ content() }}

    {{ javascript_include('node_modules/angular/angular.min.js') }}
    {{ javascript_include('node_modules/angular-animate/angular-animate.min.js') }}
    {{ javascript_include('node_modules/angular-touch/angular-touch.min.js') }}
    {{ javascript_include('node_modules/angular-sanitize/angular-sanitize.min.js') }}
    {{ javascript_include('node_modules/bootstrap/dist/js/bootstrap.min.js') }}
    {{ javascript_include('node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js') }}
    {{ javascript_include('js/app.js') }}
    {{ javascript_include('js/wholesaler.js') }}

</body>

</html>
