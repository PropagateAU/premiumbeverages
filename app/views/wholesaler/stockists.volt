<div class="container">
    <div class="title-wrapper title-wrapper--border-bottom">
        <h2>Where To Buy - Stockists</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h1>Australian &<br/>New Zealand<br/>Stockists</h1>
        </div>
    </div>
</div>
<section class="col-wrapper">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-6 col-md-4">
                <div class="stockists-image-wrapper">
                <img src="/img/stockists/bws-logo.png">
                </div>
            </div>
            <div class="col-xs-6 col-md-4">
                <div class="stockists-image-wrapper">
                <img src="/img/stockists/bws-logo.png">
                </div>
            </div>
            <div class="col-xs-6 col-md-4">
                <div class="stockists-image-wrapper">
                    <img src="/img/stockists/bws-logo.png">
                </div>
            </div>
        </div>
    </div>
</section>