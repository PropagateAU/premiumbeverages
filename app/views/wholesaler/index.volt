<main class="col-wrapper">
    <div class="container">
        <div clas="row">
            <div class="title-wrapper title-wrapper--border-bottom">
                <h2>Where To Buy</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-4 col-padding">
                <h1></h1>
                <p>We proudly manage and distribute the revered stable of Coopers products along with a hand picked portfolio that includes a selection of the world’s best beer and cider brands.</p>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="row">
                    <!-- <div class="col-xs-12">
                        <h1>FIND <br/>STOCKIST</h1>
                        <p>Premium Beverages Brands can be found all over Australia and New Zeland from a variety of stockists. To find out where to find Premium Beverages products click below.</p>
                        <div class="btn-wrapper">
                            <a href="wholesaler/stockists">Find Stockists</a>
                        </div>
                    </div> -->

                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h1>FIND <br/>WHOLESALER</h1>
                        <p>Wholesalers of Premium Beverages Brands can be found all over Australia and New Zeland. Click the map to find a wholesaller in your area.</p>
                        <div class="btn-wrapper">
                            <a href="wholesaler/{{country}}">Find Wholesaler</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>