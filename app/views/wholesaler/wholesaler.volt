<div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="title-wrapper title-wrapper--border-bottom">
                    <h2>Where To Buy - Wholesalers</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="text-wrapper text-wrapper--wholesaler">
                    <h1>FIND A WHOLESALER</h1>
                    <p>Wholesalers of Premium Beverages Brands can be found all over Australia and New Zealand. Click the map to find a wholesaler in your area.</p>
                    <div class="btn-wrapper">
                        <a ng-click="findAU()" class="">Find Australian Wholesalers</a>
                    </div><br/>
                    <div class="btn-wrapper">
                        <a ng-click="findNZ()" class="">Find New Zealand Wholesalers</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 hidden-md hidden-lg">
                <i class="zmdi zmdi-info-outline"></i>  <small>Double click on state to view the wholesalers.</small>
            </div>
            <div class="col-xs-12 col-md-6">
                <div id="map-au"></div>
                <div id="map-nz"></div>
            </div>
        </div>
    </div>
    <div id="wholesaler-results" class="col-wrapper" ng-if="wholesalers.length > 0">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="title-wrapper--border-top title-wrapper">
                        <h3>[[country]] <span ng-if="state"> - [[state]]</span><br/>
                        <span class="font-light">Wholesalers</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-md-4 wholesaler-results-wrapper" ng-repeat="wholesaler in wholesalers track by $index">
                        <h4>[[wholesaler.name]]</h4>
                        <p>
                            <span ng-if="wholesaler.phone"><span class="icon-wrapper icon-telephone"></span>
                                <span ng-if="countryCode != 'au' && wholesaler.country == 'AU'">+61</span>
                                <span ng-if="countryCode != 'nz' && wholesaler.country == 'NZ'">(0011)</span>
                                [[wholesaler.phone]]<br/>
                            </span>
                            <span ng-if="wholesaler.email"><span class="icon-wrapper icon-envelope-open"></span> [[wholesaler.email]]<br/></span>
                            <span ng-if="wholesaler.website"><span class="icon-wrapper icon-mouse-network"></span> <a ng-href="//[[wholesaler.website]]" target="_blank">[[wholesaler.website]]</a></span>
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-4 col-padding" ng-if="!wholesalers.length">
                        <p>
                            No Wholesalers found.
                        </p>
                    </div>
            </div>
        </div>
    </div>