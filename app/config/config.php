<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

require APP_PATH . '/vendor/autoload.php';

// Load getenvironment
(new \Dotenv\Dotenv(APP_PATH))->load();

return new \Phalcon\Config(array(
    'database'    => array(
        'adapter'  => getenv('DB_ADAPTER'),
        'host'     => getenv('DB_HOST'),
        'username' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'dbname'   => getenv('DB_DATABASE'),
        'charset'  => getenv('DB_CHARSET'),
    ),
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => getenv('APP_BASE_URI'),
        'publicUrl'      => getenv('APP_STATIC_URL'),
        'country'        => getenv('APP_COUNTRY'),
        'auUrl'          => getenv('AU_URL'),
        'nzUrl'          => getenv('NZ_URL'),

    ),
    'mail'        => array(
        'fromName'            => getenv('MAIL_FROM_NAME'),
		'fromEmail'           => getenv('MAIL_FROM_ADDRESS'),
		'toName'              => getenv('MAIL_TO_NAME'),
        'toEmail'             => getenv('MAIL_TO_ADDRESS'),
        getenv('MAIL_DRIVER') => array(
            'server'   => getenv('MAIL_HOST'),
            'port'     => getenv('MAIL_PORT'),
            'security' => getenv('MAIL_ENCRYPTION'),
            'username' => getenv('MAIL_USERNAME'),
            'password' => getenv('MAIL_PASSWORD'),
        ),
    ),
));
