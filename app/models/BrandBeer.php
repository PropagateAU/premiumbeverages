<?php

class BrandBeer extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $brand_id;

    /**
     *
     * @var integer
     */
    public $beer_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('beer_id', 'Beer', 'id', array('alias' => 'Beer'));
        $this->belongsTo('brand_id', 'Brand', 'id', array('alias' => 'Brand'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandBeer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BrandBeer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'brand_beer';
    }

}
