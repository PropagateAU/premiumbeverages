<?php

class Beer extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $slug;

    /**
     *
     * @var string
     */
    public $tagline;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var string
     */
    public $stats_alc_per_vol;

    /**
     *
     * @var string
     */
    public $stats_alc_per_vol_unit;

    /**
     *
     * @var string
     */
    public $stats_vol;

    /**
     *
     * @var string
     */
    public $stats_vol_unit;

    /**
     *
     * @var string
     */
    public $stats_pack_label;

    /**
     *
     * @var string
     */
    public $stats_pack;

    /**
     *
     * @var string
     */
    public $stats_large_vol;

    /**
     *
     * @var string
     */
    public $stats_large_vol_unit;

    /**
     *
     * @var string
     */
    public $stats_large_pack_label;

    /**
     *
     * @var string
     */
    public $stats_large_pack;

    /**
     *
     * @var string
     */
    public $stats_keg_vol;

    /**
     *
     * @var string
     */
    public $stats_keg_vol_unit;

    /**
     *
     * @var string
     */
    public $stats_keg_label;

    /**
     *
     * @var string
     */
    public $roll_bottle;

    /**
     *
     * @var string
     */
    public $roll_bottle_image;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'BrandBeer', 'beer_id', array('alias' => 'BrandBeer'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Beer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Beer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'beer';
    }

}
