# Adding beer to the database
- Add beer details to the beer table. Mark `au` as 1 if the beer is to be displaye in Australia, `nz` as 1 if the beer is to be displaye in New Zealand.
- If the beer details differ in two countries, create a copy of the beer and mark the correct country as 1.